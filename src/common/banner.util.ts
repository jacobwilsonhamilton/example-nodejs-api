import chalk from 'chalk';
import figlet from 'figlet';

export const banner = (text: string): void => {
    // eslint-disable-next-line import/no-named-as-default-member
    consoleYellow(figlet.textSync(text, { font: 'Computer', horizontalLayout: 'default' }));
};

export const consoleRed = (text: string): void => {
    console.log(chalk.red(text));
};

export const consoleGreen = (text: string): void => {
    console.log(chalk.green(text));
};

export const consoleYellow = (text: string): void => {
    console.log(chalk.yellow(text));
};

export const consoleMagenta = (text: string): void => {
    console.log(chalk.magenta(text));
};
