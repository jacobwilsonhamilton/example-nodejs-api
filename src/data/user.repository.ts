import { join, dirname } from 'path';
import { Low, JSONFile } from 'lowdb';
import { fileURLToPath } from 'url';
import { User } from '@common/models/user.model';
import { consoleGreen } from '@common/banner.util';

const __dirname = dirname(fileURLToPath(import.meta.url));

export type Data = {
    users: User[];
};

const file = join(__dirname, 'db.json');
const adapter = new JSONFile<Data>(file);
const db = new Low(adapter);

export const initializeDb = async () => {
    await db.read();

    consoleGreen(`writing data to ${__dirname}`);

    db.data = db.data || { users: [] };

    consoleGreen(`database initialized with ${db.data.users.length} number of records`);
};

export const getUserByUserName = (userName: string): Partial<User> => {
    const user = db.data?.users.find(x => x.userName === userName) || {};
    return {
        ...user,
        password: undefined,
    };
};

export const upsertUser = async (user: User): Promise<void> => {
    db.data!.users = [...(db.data?.users.filter(_ => _.userName !== user.userName) || []), user];

    await db.write();
};
