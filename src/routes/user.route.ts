import { getUserByUserName, upsertUser } from '@data/user.repository';
import { Router } from 'express';

const router = Router();

router.route('/user/:userName').get((request, response) => {
    response.json(getUserByUserName((request.params as any).userName));
});

router.route('/user').post(async (request, response) => {
    console.log(request.body);
    await upsertUser(request.body);
    response.status(200).send();
});

export const userRouter = router;
