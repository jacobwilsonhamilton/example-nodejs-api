import webpack from 'webpack';
import nodeExternals from 'webpack-node-externals';
import TsconfigPathsPlugin from 'tsconfig-paths-webpack-plugin';
import path from 'path';

const __dirname = path.resolve();

export default {
    mode: 'production',
    entry: './src/server.ts',
    output: {
        path: __dirname + '/bin',
        filename: 'server.js',
        libraryTarget: 'module',
        chunkFormat: 'module',
    },
    experiments: {
        outputModule: true,
        topLevelAwait: true,
    },
    externalsPresets: { node: true },
    externalsType: 'module',
    externals: [
        nodeExternals({
      importType: 'module'
    })
    ],
    plugins: [
        new webpack.BannerPlugin({ banner: '#!/usr/bin/env node', raw: true })
    ],
    module: {
        rules: [
            {
                test: /\.conf/,
                type: 'asset/source'
            },
            {
                test: /\.tsx?$/,
                use: [
                    { loader: 'shebang-loader' },
                    {
                        loader: 'ts-loader',
                        options: {
                            transpileOnly: false
                        },
                    }
                ]
            }
        ]
    },
    resolve: {
        plugins: [
            new TsconfigPathsPlugin({})
        ],
        extensions: ['.ts', '.tsx', '.js']
    }
};
